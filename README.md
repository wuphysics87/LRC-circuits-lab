# LRC Circuits Lab
## Theory
Often in physics we find analogs between seemingly different systems.  The LRC circuit is an example.

### Mass on Spring with Dampener

Consider the following system.


![FBD](images/FBD.png "image0")

Drawing a Free body and applying Newton's Second Law we see:

$$m\frac{d^2x}{dt^2} = -\beta v - kx$$

Rearranging and applying $\frac{dx}{dt} = \dot{x}$ and $\frac{d^2x}{dt^2} = \ddot{x}$ we ca write this as:

$$m\ddot{x} + \beta \dot{x} + k x = 0$$

We have some physical intution of what this equations is describing if we consider a screen door with a spring to dampen it from slamming too hard. If the dampener is strong, it closes slowly.  If the dampener is exactly not hard enough, it slams hard, and if the dampener is too weak, we get something like a saloon door which swings back and forth until it closes.


### LRC Circuit
Consider the following LRC circuit without the function generator with a charge on the upper plate.
![LRC](images/LRC.jpg "LRC")

We can determine the charge as a function of time by applying Faraday's Law:

$$\oint \vec{E} \cdot d\vec{l} = -\frac{d}{dt} \int \vec{B} \cdot d\vec{A} \pm L \frac{dI}{dt} $$

Going around the loop and noting there is no external magnetic field, we see that:

$$IR + \frac{1}{C} Q = - L \frac{dI}{dt}$$

Rearranging and using  $\frac{dQ}{dt} = \dot{Q}$ and $\frac{d^2Q}{dt^2} = \ddot{Q}$ we ca write this as:

$$L\ddot{Q} + R \dot{Q} + \frac{1}{C} Q = 0$$

To solve this we make the substitutions $2 \beta = \frac{R}{L}$ and $\omega^2 = \frac{1}{LC}$.

$$\ddot{Q} + 2\beta \dot{Q} + \omega^2 Q = 0$$

We now guess at a solution to the equation:

$$ Q(t) = Q_0 e^{r * t} $$

Substituting into our differnetial equation noting $\ddot{Q} = r^2 Q(t)$ and $\dot{Q}= r Q(t)$ we find:

$$ (r^2 + 2\beta + \omega^2)Q(t) =0 $$

$r$ can be solved for by applying the quadratic formula.  The above equation is satisfied provided that:

$$r_{\pm} = -\beta \pm \sqrt{\beta^2 -\omega^2}$$


## Experiment
Now we will see the case where $\beta^2 \lt \omega^2$ experimentally. First, create the circuit as shown in the following images:
![image 1](images/image-1.jpeg "image1")
![image 2](images/image-2.jpeg "image1")
![image 3](images/image-3.jpeg "image1")

Next open Pasco with the template provided in this repository (LRC-circuit-template.cap). Click the run button to generate the plot.  Take a screenshot and paste it into OneNote

## Code
The final portion of this lab is to write code to produce an interactive plot showing all three situations. This code is mostly pre-written, but requires modification to run. Open it in Codium to begin editing.  To get the code to run correctly, you will need to:

1. Import the correct packages (Hint: look at `requirements.txt`)
2. Enter the correct equations for `beta` and `omega` in the `v_t` function.
3. Add the correct labels for the plot

Once you have done all of these three things, you will be greeted with an interactive plot.

## Uploading Your Code

In order to recieve credit for this lab, you will need to create a GitLab account, push it to GitLab, and send me the link once you have made it public.  This will require you to complete:

1. [Hello-Bitwarden](https://gitlab.com/wuphysics87/hello-bitwarden)
1. [Hello-Gitlab](https://gitlab.com/wuphysics87/hello-gitlab)

Not only will these net you the points for the lab, they are also worth 10 points a piece for your computing extra credit.
