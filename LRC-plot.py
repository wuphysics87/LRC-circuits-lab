# Replace PACKAGE1 and PACKAGE2 with the appropriate packages
# HINT: Check requirements.txt

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button


# The parametrized function to be plotted
def v_t(t, V_0, R_0, L_0, C_0):
    # Replace BETA and OMEGA with their appropriate equations
    # Note: with numpy, a square root is written as numpy.sqrt
    # Make sure you are using the same namespace as your import statements!
    beta=R_0/(2*L_0)
    omega=np.sqrt(1/(L_0 * C_0))

    if beta >= omega:
        return V_0 * np.exp(-beta * t) * np.cosh(np.sqrt(np.absolute(pow(beta,2) - pow(omega,2)))*t)
    else:
        return V_0 * np.exp(-beta * t) * np.cos(np.sqrt(np.absolute(pow(beta,2) - pow(omega,2)))*t)

# Define initial parameters
t = np.linspace(0, 100, 1000)
R_0 = 10
L_0 = .1
C_0 = 2
V_0 = 5

# Create the figure and the line that we will manipulate
fig, ax = plt.subplots()
plt.ylim([-5,5])
line, = ax.plot(t, v_t(t, V_0, R_0, L_0, C_0), lw=1)
ax.set_xlabel('I need a label')

# adjust the main plot to make room for the sliders
fig.subplots_adjust(left=0.25, bottom=0.25)

# Make a vertically oriented slider to control the amplitude
axV = fig.add_axes([0.1, 0.25, 0.0225, 0.63])
V_slider = Slider(
    ax=axV,
    label="Voltatge",
    valmin=0,
    valmax=10,
    valinit=V_0,
    orientation="vertical"
)

# Make a horizontal sliders to control L, R, and C
axL = fig.add_axes([0.25, 0.05, 0.65, 0.03])
L_slider = Slider(
    ax=axL,
    label='L',
    valmin=.01,
    valmax=.5,
    valinit=L_0,
)

axR = fig.add_axes([0.25, 0.10, 0.65, 0.03])
R_slider = Slider(
    ax=axR,
    label='R',
    valmin=.1,
    valmax=1,
    valinit=R_0,
)

axC = fig.add_axes([0.25, 0.15, 0.65, 0.03])
C_slider = Slider(
    ax=axC,
    label='C',
    valmin=1,
    valmax=50,
    valinit=C_0,
)

# The function to be called anytime a slider's value changes
def update(val):
    line.set_ydata(v_t(t, V_slider.val, L_slider.val, R_slider.val, C_slider.val))
    fig.canvas.draw_idle()


# register the update function with each slider
V_slider.on_changed(update)
L_slider.on_changed(update)
R_slider.on_changed(update)
C_slider.on_changed(update)

# Create a `matplotlib.widgets.Button` to reset the sliders to initial values.
resetax = fig.add_axes([0.1, 0.15, 0.1, 0.04])
button = Button(resetax, 'Reset', hovercolor='0.975')


def reset(event):
    V_slider.reset()
    L_slider.reset()
    R_slider.reset()
    C_slider.reset()
button.on_clicked(reset)

plt.show()
